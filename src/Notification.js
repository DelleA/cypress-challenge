import React from 'react';

const Notification = ({ message, type }) => {
  if (message === null) {
    return null
  }
  
  return (
    <div 
      className={"bg-tableBackground text-center text-xl " + (type === 'error' ? 'text-red-700' : 'text-green-700')}>
      {message}
    </div>
  );
}

export default Notification;