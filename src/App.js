import React, { useState }  from "react";
import Notification from './Notification'
import './css/tailwind.css'

const App = () => {

  const [ username, setUsername ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ notification, setNotification ] = useState(null)
  
  const handleSubmit = event => {
    event.preventDefault();
    if (username === 'admin' && password === 'admin') {
      setNotification({
        type: 'success',
        message: 'Welcome, OverLord Kahn'
      })
    } else {
      setNotification({
        type: 'error',
        message: 'Wrong credentials'
      })
    }
  };

  return (
    <div className="flex bg-blue-500 min-h-screen">
      <div className="sm:w-1/3">.</div>
      <div className="pt-6 mt-24 text-center w-1/3">
        <div className="bg-gray-700 py-8 rounded">
          <p className="mb-6 text-xl">LOG IN</p>
          <div className="pb-4">
            <Notification {...notification}/>
          </div>
          <form onSubmit={handleSubmit}>
            <p>Username</p>
            <input
              onChange={event => setUsername(event.target.value)}
              className="rounded px-2"
              label="Username"
              name="username"
            />
            <p>Password</p>
            <input
              type="password"
              onChange={event => setPassword(event.target.value)}
              className="rounded px-2"
              label="password"
              name="password"
            />
            <div className="mt-4">
              <button className="bg-black text-white px-4 py-2 rounded" name="submit" type="submit">Submit</button>
            </div>
            
          </form>
        </div>
        
      </div>
      <div className="w-1/3"></div>
    </div>
    
  );
};

export default App;