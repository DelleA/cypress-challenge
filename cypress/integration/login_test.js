///<reference types="Cypress"/>

//type in username
//click on submit button
//check that new component is rendered
//  wrong creds: check component shows error
//  right cred: check component correct

describe('Login', () => {
  it('logs in successfully with the right credentials', () => {
    cy.visit('localhost:3000')
    cy.get('input[name=username]').type('admin');
    cy.get('input[name=password]').type('admin');
    cy.get('button[name=submit]').click();
    cy.contains('Welcome')
  })

  it('fails to log in with the wrong credentials', () => {
    cy.visit('localhost:3000')
    cy.get('input[name=username]').type('wrong');
    cy.get('input[name=password]').type('credentials');
    cy.get('button[name=submit]').click();
    cy.contains('Wrong credentials')
  })
})